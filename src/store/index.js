import Vue from 'vue';
import Vuex from 'vuex';

import global from './global';
import time from './time';

Vue.use(Vuex);

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation
 */

export default function(/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      global,
      time,
    },
  });

  Store.dispatch('time/start');
  return Store;
}
