export default {
  namespaced: true,
  state: {
    a: 1,
    now: Date.now(),
  },
  actions: {
    start({ state }) {
      setInterval(() => {
        state.now = Date.now();
      }, 1000);
    },
  },
};
