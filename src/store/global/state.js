export default {
  socket: {},
  player: {},
  fetched: { planets: [] },
  fleets: [],
  fleetsFetched: false,
  isLogged: false,
  authBuffer: 0,
  keepAlive: {},
};
