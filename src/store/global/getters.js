import { prodBuffs } from 'shared/sp-shared';

export function isLogged(state) {
  return state.isLogged;
}

export const getPlanet = state => index => {
  const p = state.player.planets[index];
  // What is fn.call https://stackoverflow.com/a/9812721
  p.buffs = prodBuffs.call(p);
  return p;
};
