import { Loading, Notify } from 'quasar';
import {
  AuthReq,
  RequestMsg,
  ResponseMsg,
  PlayerInfo,
  PlanetInfo,
} from 'shared/sp-proto';

export function initSocket(state, auth) {
  // Encode Auth
  const error = AuthReq.verify(auth);
  if (error) throw error;
  const buffer = AuthReq.encode(auth).finish();
  state.authBuffer = RequestMsg.encode({
    id: RequestMsg.Id.AuthReq,
    content: buffer,
  }).finish();

  // Make sure no socket is opened
  try {
    state.socket.close();
  } catch (e) {
    console.log('socket not opened yet');
  }

  // Connect
  state.socket = new WebSocket('wss://zyntuz.dev/wss');
  state.socket.binaryType = 'arraybuffer';
}

export function WS_ONOPEN(state) {
  state.socket.send(state.authBuffer);
  state.keepAlive = setInterval(() => {
    state.socket.send(new Uint8Array(1));
  }, 25000);
}

export function WS_ONCLOSE(state) {
  clearInterval(state.keepAlive);
  state.isLogged = false;
  Notify.create('连接以断开! 请刷新或重新登录');
}

export function WS_ONMESSAGE(state, event) {
  const foo = new Uint8Array(event.data);
  const reply = ResponseMsg.decode(foo);
  console.log(`Received: ${ResponseMsg.Id[reply.id]}`);
  switch (reply.id) {
    case ResponseMsg.Id.ErrorRes: {
      const error = ResponseMsg.ErrorId[reply.error];
      Notify.create(`失败：${error}`);
      Loading.hide();
      break;
    }
    case ResponseMsg.Id.PlayerInfo: {
      const info = PlayerInfo.decode(reply.content);
      state.player = { ...state.player, ...info };
      state.isLogged = true;
      Notify.create('登录成功！');
      Loading.hide();
      this.$router.push({ path: '/' });
      break;
    }
    case ResponseMsg.Id.PlanetInfo: {
      const info = PlanetInfo.decode(reply.content);
      state.player.planets.splice(info.index, 1, {
        ...state.player.planets[info.index],
        ...info,
      });
      Loading.hide();
      break;
    }
    // case 'planet':
    //   state.planets.splice(data.shift(), 1, data);
    //   Notify.create('操作成功！');
    //   Loading.hide();
    //   this.$router.push({ path: '/planets' });
    //   break;

    // case 'pd': {
    //   const index = data.shift();
    //   state.planets[index].splice(3);
    //   state.planets[index].push(...data);
    //   Loading.hide();
    //   break;
    // }

    // case 'fleets': {
    //   [state.fleets] = data;
    //   state.fleetsFetched = true;
    //   Loading.hide();
    //   break;
    // }

    // case 'fd': {
    //   const index = data.shift();
    //   state.fleets[index].splice(2);
    //   state.fleets[index].push(...data);
    //   Loading.hide();
    //   break;
    // }

    default:
      break;
  }
}
