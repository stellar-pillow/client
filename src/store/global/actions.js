import { Loading } from 'quasar';
import PbRoot, { RequestMsg } from 'shared/sp-proto';

export function login(ctx, auth) {
  Loading.show();
  ctx.commit('initSocket', auth);
  ctx.state.socket.onopen = () => ctx.commit('WS_ONOPEN');
  ctx.state.socket.onmessage = event => ctx.commit('WS_ONMESSAGE', event);
  ctx.state.socket.onclose = event => ctx.commit('WS_ONCLOSE', event);
}

export function sendReq(ctx, { id, obj }) {
  Loading.show();
  const Request = PbRoot[RequestMsg.Id[id]];
  const error = Request.verify(obj);
  if (error) throw error;
  const msg = RequestMsg.encode({
    id,
    content: Request.encode(obj).finish(),
  }).finish();
  ctx.state.socket.send(msg);
}

export function setCredits(ctx, newCredit) {
  ctx.state.player.credits = newCredit;
}
