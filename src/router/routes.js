import PageAccount from 'pages/Account.vue';
import PlanetList from 'pages/PlanetList.vue';
import PlanetView from 'pages/PlanetView.vue';
import PlanetBuy from 'pages/PlanetBuy.vue';
import FleetList from 'pages/FleetList.vue';
import FleetView from 'pages/FleetView.vue';
import PageWiki from 'pages/Wiki.vue';
import PlayerProfile from 'pages/PlayerProfile.vue';

const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') },
      { path: '/player', component: PlayerProfile },
      { path: '/planets', component: PlanetList },
      { path: '/planets/buy', component: PlanetBuy },
      { path: '/planets/:id', component: PlanetView },
      { path: '/fleets', component: FleetList },
      { path: '/fleets/:id', component: FleetView },
      { path: '/wiki', component: PageWiki },
    ],
  },
  {
    path: '/account',
    component: PageAccount,
    meta: { guest: true },
  },
];

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue'),
  });
}

export default routes;
