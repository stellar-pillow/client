import { structs } from 'shared/sp-data';

export const toFormattedTime = milliseconds => {
  const sec = Math.round(milliseconds / 1000);
  const d = Math.floor(sec / (3600 * 24));
  const h = Math.floor((sec % (3600 * 24)) / 3600);
  const m = Math.floor((sec % 3600) / 60);
  const s = Math.floor((sec % 3600) % 60);
  const dF = d ? `${d}天` : '';
  const hF = h ? `${h}小时` : '';
  const mF = m ? `${m}分钟` : '';
  const sF = s ? `${s}秒` : '';
  return dF + hF + mF + sF;
};

/**
 *  Adjust color (lighter, darker)
 * @param {string} color in #FFFFFF format
 * @param {number} step
 * @returns {string} color
 */
export const adjustColor = (color, step) => {
  const rgb = [];
  rgb[0] = parseInt(color.substring(1, 3), 16) + step;
  rgb[1] = parseInt(color.substring(3, 5), 16) + step;
  rgb[2] = parseInt(color.substring(5, 7), 16) + step;

  for (let i = 0; i < 3; i += 1) {
    if (rgb[i] > 255) rgb[i] = 255;
    else if (rgb[i] < 0) rgb[i] = 0;
  }

  return `rgb(${rgb[0]},${rgb[1]},${rgb[2]})`;
};

/**
 * output struct cost
 * @param {number} id
 * @param {number} level
 * @returns {string} cost
 */
export const structCost = (id, level) => {
  const {
    pop, metal, crystal, ms,
  } = structs[id].cost(level);
  return `人口: ${pop}, 金属: ${metal}, 水晶: ${crystal}, 耗时: ${toFormattedTime(ms)}`;
};
